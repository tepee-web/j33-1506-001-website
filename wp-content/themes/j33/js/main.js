(function($) {


  $("a.next-step").on("click", function(e){
    e.preventDefault();

    $('html, body').animate({
      scrollTop: 0
    }, 200);
    
    if( $(this).text() == "Go Back" ){
      $(".step.active").removeClass("active").prev(".step").addClass("active");

      $(".form-section").css('display', 'none');
      $(this).parents(".form-section").prev(".form-section").fadeIn();
    }
    else{
      var number = $(this).attr("id");
      var activeStep = "#step-" + number; 
      $(".step").removeClass("active");
      $(activeStep).addClass("active");

      var targetSection = "." + number;
      $(".form-section").css('display', 'none');
      $(targetSection).fadeIn();
    }
  });
	
  /*
    Setting twitter bootstraps scrollspy
    to active highlight menu items
  */
  $('body').scrollspy({ target: '#scrollspy', offset: 200 });

  // if( !$('body').hasClass("home") ){
  //   $(".scroll-links a").each(function(){
  //     $(this).attr("href", "/").parent(".scroll-links").removeClass("scroll-links");
  //   });
  // }


  $('#tabs li a').on("click", function (e) {
    e.preventDefault()
    $(this).tab('show')
  })

  
  /*
    menu click scroll to section
  */

  function scrollTo(target){
    var windowWidth = $(window).width();
    var offset = windowWidth > 767 ? 140 : 40;

    $('html, body').animate({
        scrollTop: $(target).offset().top - offset
    }, 2000);

    if( $(window).width() < 768 ){
      $('#scrollspy')
          .removeClass('in')
          .addClass('collapse')
          .css('height','1px');
    }
  }

  var hash = window.location.hash;
  if(hash){
    var target = hash;
    scrollTo(target);
  }
  $('.nav li.scroll-links a').on('click', function(e){
      
      var target = $(this).attr('href');
      
      if( $("body").hasClass("home") ){
        e.preventDefault();

        $(".nav li.scroll-links").removeClass("active");
        $(this).parent("li.scroll-links").addClass("active");

        scrollTo(target);

      }
      else{

        window.location.href="/"+target;
      }
  });

  $(".scroll-to-contact").on('click', function(e){
    e.preventDefault();

    var windowWidth = $(window).width();
    var offset = windowWidth > 767 ? 140 : 20;
    $('html, body').animate({
          scrollTop: $('footer').offset().top - offset
      }, 2000);

    var section = $(this).parents("article").attr("id");
    var subject = $(this).text();
    $( "input[name='subject']" ).val(section + " - " + subject);

    $("input[name='name']").focus();

  });

  $("a.banner-section").on('click', function(e){
      e.preventDefault();

      var windowWidth = $(window).width();
      var offset = windowWidth > 767 ? 140 : 20;
      var target = $(this).attr('href');
      $('html, body').animate({
          scrollTop: $(target).offset().top - offset
      }, 2000);
  });

  /*
    Functionaility wit swipe
    for testimonials carousel
  */
  $('#main-carousel').carousel({
    interval: 3000
  });

  $(".carousel").swiperight(function() {  
    $(this).carousel('prev');  
  });  
  $(".carousel").swipeleft(function() {  
    $(this).carousel('next');  
  }); 


  /*
    Functions triggered on browser
    window resize
  */
  $(window).resize(function () {

  });

  /*
    Modernizr - Add placeholders to browsers with no support
  */

  if(!Modernizr.input.placeholder) {
      $("input[placeholder]").each(function() {
          var placeholder = $(this).attr("placeholder");

          $(this).val(placeholder).focus(function() {
              if($(this).val() == placeholder) {
                  $(this).val("")
              }
          }).blur(function() {
              if($(this).val() == "") {
                  $(this).val(placeholder)
              }
          });
      });
  }


})(jQuery);