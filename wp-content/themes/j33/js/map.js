function initializeMap() {

    var MY_MAPTYPE_ID = 'custom_style';

    var featureOpts = [

  {
    "stylers": [
      { "weight": 2 },
      { "visibility": "simplified" },
      { "saturation": -100 }
    ]
  }

  ];
    
    // set Zoom Dependant on window width
    var windowWidth = window.innerWidth;
    if( windowWidth > 767){
      var zoom = 8;
    }
    else{
      var zoom = 5;
    }

    var myLatlng = new google.maps.LatLng(54.4362495, -6.2992401);
    var mapOptions = {
        zoom: zoom,
        scrollwheel: false,
        //draggable: false,
        center: myLatlng,
        disableDefaultUI: true,
        mapTypeIds: [google.maps.MapTypeId.ROADMAP, MY_MAPTYPE_ID],
        mapTypeId: MY_MAPTYPE_ID
    }

    var map = new google.maps.Map(document.getElementById("google-map"), mapOptions);

    var styledMapOptions = {
        name: 'Custom Style'
    };

    var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);

    map.mapTypes.set(MY_MAPTYPE_ID, customMapType);

    if( windowWidth > 767){
      var image = '/wp-content/themes/headspace/img/map-marker.png';
    }
    else{
      var image = '/wp-content/themes/headspace/img/map-marker.png';
    }
    

    var markers = [];


    if($(".location").length > 0){

      var sites = [];

      var title = $(".location").find("p.title").text();
      var address = $(".location").find("p.content").html();
      var address_html = '<strong style="font-weight: bold; display: block; color: #9c223f;">'+title+'</strong><hr style="margin-top: 4px; margin-bottom: 4px;"/>'+address+'</p>';
      var coord_lat = 54.4362495;
      var coord_long = -6.2992401;
      var site = [ title, coord_lat, coord_long, address_html ];
      sites.push(site);

    }


    setMarkers(map, sites);
      infowindow = new google.maps.InfoWindow({
                content: "loading..."
            });

    function setMarkers(map, markers) {

        for (var i = 0; i < markers.length; i++) {
            var sites = markers[i];
            var siteLatLng = new google.maps.LatLng(sites[1], sites[2]);
            var marker = new google.maps.Marker({
                position: siteLatLng,
                map: map,
                title: sites[0],
                html: sites[3],
                icon: image
            });

            var contentString = "Some content";

            google.maps.event.addListener(marker, "click", function () {
                infowindow.setContent(this.html);
                infowindow.open(map, this);
            });
        }
    }

    google.maps.event.addDomListener(window, 'resize', function() {
        map.setCenter(myLatlng);
    });

    

}