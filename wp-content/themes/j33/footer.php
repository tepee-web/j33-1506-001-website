  <!--
    FOOTER
  -->

    <?php
      $email = get_field('email', 'option');
      $facebook_link = get_field('facebook_link', 'option');
      $twitter_link = get_field('twitter_link', 'option');
    ?>

  	<footer>
  	  <div class="container center">
        <div class="col-xs-12">
          <h3><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></h3>
        </div>
        <!--
        <div class="col-xs-6 right">
          <ul class="social">
            <li class="fb"><a target="_blank" title="View Our facebook Page" href="<?php echo $facebook_link; ?>">facebook</a></li>
            <li class="tw"><a target="_blank" title="View Our twitter Page" href="<?php echo $twitter_link; ?>">twitter</a></li>
          </ul>
        </div>
        -->
      </div>
  	</footer>

  	<!-- jQuery & Bootstrap Libraries -->
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/jquery.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/jquery-mobile.min.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/vendor/modernizr.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js"></script>
    
	   <!-- Created by Tepee Design -->
    <script src="<?php echo get_template_directory_uri(); ?>/js/map.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>

    <?php wp_footer(); ?>
  </body>
</html>