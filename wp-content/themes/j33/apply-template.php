<?php     
/*
Template Name: Apply
*/ 
?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

  <section id="main">

    <article id="default-title-area">
       <div class="container">
        <div class="col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8">
          <h1><?php the_title(); ?></h1>
        </div>
       </div>
    </article>

    <!-- Contact Form -->
    <article id="contact-form">

<!--       <div class="container center">
        <h3>Grant Application Form</h3>

        <div class="application-steps">
          <div class="steps-line"></div>

          <div class="step active" id="step-one">
            <div class="step-number">1</div>
            <div class="step-label">
              Step 1<br>
              Your Organisation's Information
            </div>
          </div>

          <div class="step" id="step-two">
            <div class="step-number">2</div>
            <div class="step-label">
              Step 2<br>
              Organisation's Financial Information
            </div>
          </div>

          <div class="step" id="step-three">
            <div class="step-number">3</div>
            <div class="step-label">
              Step 3<br>
              Your Organisation's Request
            </div>
          </div>

          <div class="step" id="step-four">
            <div class="step-number">4</div>
            <div class="step-label">
              Step 4<br>
              Declaration
            </div>
          </div>

        </div>

      </div>
       -->
      <?php the_content(); ?>
      
     <!--  <form>

        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
          
          <div class="panel panel-default">  
            <div class="panel-heading" role="tab" id="headingOne">
              <div class="container">
                <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Your Organisation’s Information: <span>Help us know you!</span></a></h4>
              </div>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                <div class="container">

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="organisationName">Name Of Organisation</label>
                        <input type="text" class="form-control" id="organisationName">
                      </div>
                      <div class="form-group">
                        <label for="organisationAddress">Organisation Address</label>
                        <textarea id="organisationAddress" class="form-control"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="fullName">Full Name Of Contact Person Making The Application</label>
                        <input type="text" class="form-control" id="fullName">
                      </div>
                      <div class="form-group">
                        <label for="positionHeld">Position Held In Organisation</label>
                        <input type="text" class="form-control" id="positionHeld">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="boardOfDirectors">Names Of The Organisation's Board Of Directors</label>
                        <input type="text" class="form-control" id="boardOfDirectors">
                      </div>
                      <div class="form-group">
                        <label for="website">Website</label>
                        <input type="text" class="form-control" id="website">
                      </div>
                      <div class="form-group">
                        <label for="charityNumber">Registered Charity Number (If Applicable)</label>
                        <input type="email" class="form-control" id="charityNumber">
                      </div>
                      <div class="form-group">
                        <label for="email">Contact Email</label>
                        <input type="email" class="form-control" id="email">
                      </div>
                      <div class="form-group">
                        <label for="numbers">Phone Numbers</label>
                        <input type="text" class="form-control" id="numbers">
                      </div>
                      <div class="form-group">
                        <label for="organisationLeaders">Name/s of the Organisation's Leader/s</label>
                        <input type="text" class="form-control" id="organisationLeaders">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="aimsAndObjectives">Aims and Objectives of the Organisation</label>
                        <small>(include target age-group/s and current geographical presence)</small>
                        <textarea id="aimsAndObjectives" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="vision">What is the vision of your Organisation</label>
                        <textarea id="vision" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="mainFocus">Description Of The Organisation’s Main Focus</label>
                        <small>(Indicate areas that best describe the Ministry’s focus with a short explanation of how this is so)</small>
                        <textarea id="mainFocus" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label class="form-section-header">State % in Box</label>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-3 col-lg-2">
                      <div class="form-group">
                        <label for="discipleship">Discipleship</label>
                        <input type="text" class="form-control" id="discipleship">
                      </div>
                    </div>

                    <div class="col-sm-3 col-lg-2">
                      <div class="form-group">
                        <label for="peace">Reconciliation &amp; Peace</label>
                        <input type="text" class="form-control" id="peace">
                      </div>
                    </div>

                    <div class="col-sm-3 col-lg-2">
                      <div class="form-group">
                        <label for="prayer">Prayer</label>
                        <input type="text" class="form-control" id="prayer">
                      </div>
                    </div>

                    <div class="col-sm-3 col-lg-2">
                      <div class="form-group">
                        <label for="compassion">Compassion</label>
                        <input type="text" class="form-control" id="compassion">
                      </div>
                    </div>

                    <div class="col-sm-3 col-lg-2">
                      <div class="form-group">
                        <label for="bible">Bible</label>
                        <input type="text" class="form-control" id="bible">
                      </div>
                    </div>

                    <div class="col-sm-3 col-lg-2">
                      <div class="form-group">
                        <label for="fellowship">Fellowship</label>
                        <input type="text" class="form-control" id="fellowship">
                      </div>
                    </div>

                    <div class="col-sm-3 col-lg-2">
                      <div class="form-group">
                        <label for="localChurch">Local Church</label>
                        <input type="text" class="form-control" id="localChurch">
                      </div>
                    </div>

                    <div class="col-sm-3 col-lg-2">
                      <div class="form-group">
                        <label for="leadership">Leadership</label>
                        <input type="text" class="form-control" id="leadership">
                      </div>
                    </div>

                    <div class="col-sm-3 col-lg-2">
                      <div class="form-group">
                        <label for="evangelism">Evangelism</label>
                        <input type="text" class="form-control" id="evangelism">
                      </div>
                    </div>

                    <div class="col-sm-3 col-lg-2">
                      <div class="form-group">
                        <label for="other">Other</label>
                        <input type="text" class="form-control" id="other">
                      </div>
                    </div>

                  </div>

                  <hr />

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="partnerWith">What local churches do you currently partner with?</label>
                        <textarea id="partnerWith" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="mainActivities">Please describe your main activities together</label>
                        <textarea id="mainActivities" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label class="form-section-header">Please provide the names and contact information for official representatives of the local churches you partner with.</label>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="fullNames">Full Names</label>
                        <input type="text" class="form-control" id="fullNames">
                      </div>
                    </div>

                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="emailAddresses">Email Addresses</label>
                        <input type="text" class="form-control" id="emailAddresses">
                      </div>
                    </div>

                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="phoneNumbers">Phone Numbers</label>
                        <input type="text" class="form-control" id="phoneNumbers">
                      </div>
                    </div>
                  </div>

                  <hr />

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="otherOrganisations">What other organisations do you currently partner with?</label>
                        <textarea id="otherOrganisations" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label class="form-section-header">Organisation's Financial Information</label>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="annualIncomeLastYear">Total Annual Income (Last Year)</label>
                        <input type="text" class="form-control" id="annualIncomeLastYear">
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="annualIncomePreviousYear">Total Annual Income (Previous Year)</label>
                        <input type="text" class="form-control" id="annualIncomePreviousYear">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="annualExpenditureLastYear">Total Annual Expenditure (Last Year)</label>
                        <input type="text" class="form-control" id="annualExpenditureLastYear">
                      </div>
                    </div>

                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="annualExpenditurePreviousYear">Total Annual Expenditure (Previous Year)</label>
                        <input type="text" class="form-control" id="annualExpenditurePreviousYear">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="examinedAuditedBy">Are accounts independently examined / audited? If so, by whom? </label>
                        <input type="text" class="form-control" id="examinedAuditedBy">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="whatAccountability">If not, please explain what accountability is in place?</label>
                        <textarea id="whatAccountability" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="financiallyFunded">Describe generally how you are financially funded</label>
                        <textarea id="financiallyFunded" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>

                  <hr />

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="bankStatementName">Organisation Name on Bank Statements</label>
                        <input type="text" class="form-control" id="bankStatementName">
                      </div>
                      <div class="form-group">
                        <label for="bankAddress">Bank Address</label>
                        <textarea id="bankAddress" class="form-control"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="bankSwiftCode">Bank SWIFT/BIC Code</label>
                        <input type="text" class="form-control" id="bankSwiftCode">
                      </div>
                      <div class="form-group">
                        <label for="ibanNumber">Bank IBAN number</label>
                        <input type="text" class="form-control" id="ibanNumber">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="">
                            Does the account require the signatures of two unrelated people for any cheques?
                          </label>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="">
                            Does your Organisation hold to the following as truth?
                          </label>
                        </div>
                      </div>

                      <hr />

                      <div class="form-group">
                        <div class="row">
                          <div class="col-sm-7 col-lg-9">
                            <label>Jesus Christ is the Son of God—truly God and truly man—and sole reconciler between man and God.</label>
                          </div>
                          <div class="col-sm-5 col-lg-3">
                            <div class="radio-inline">
                              <label><input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked> Yes </label>
                            </div>
                            <div class="radio-inline">
                              <label> <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2"> No </label>
                            </div>
                          </div>
                        </div>
                      </div>

                      <hr />

                      <div class="form-group">
                        <div class="row">
                          <div class="col-sm-7 col-lg-9">
                            <label>Eternal life with God is solely by the grace of God through faith in Jesus Christ in light of His death on the cross and physical resurrection from the dead.</label>
                          </div>
                          <div class="col-sm-5 col-lg-3">
                            <div class="radio-inline">
                              <label><input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked> Yes </label>
                            </div>
                            <div class="radio-inline">
                              <label> <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2"> No </label>
                            </div>
                          </div>
                        </div>
                      </div>


                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingTwo">
              <div class="container">
                <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Your Organisation’s Request: <span>Help us help you!</span></a></h4>
              </div>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                <div class="container">

                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="ammountRequested">Amount Requested</label>
                        <input type="text" class="form-control" id="ammountRequested">
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="budgetPercentage">Total % of Annual Budget</label>
                        <input type="text" class="form-control" id="budgetPercentage">
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="summaryReason">Summary Reason for this Request</label>
                        <textarea id="summaryReason" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="expandedDescription">Expanded Description for this Request</label>
                        <textarea id="expandedDescription" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="expectedOutcomes">What are the expected outcomes you envisage for your organisation as a result of this grant.</label>
                        <textarea id="expectedOutcomes" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="measureImpact">How do you intend to measure the impact of this grant for your Organisation.</label>
                        <textarea id="measureImpact" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-sm-5">
                      <div class="form-group">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="">
                            Have you received funding from J33 previously?
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-7">
                      <div class="form-group">
                        <label for="howMuchEachTime">If so, when and how much <strong>each</strong> time.</label>
                        <textarea id="howMuchEachTime" class="form-control"></textarea>
                      </div>
                    </div>
                  </div>


                </div>
              </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading" role="tab" id="headingThree">
              <div class="container">
                <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Declaration</a></h4>
              </div>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body">
                <div class="container">
                  
                  <div class="row">
                    <div class="col-sm-6">
                      <div class="form-group">
                        <div class="checkbox">
                          <label>
                            <input type="checkbox" value="">
                            To the best of my knowledge, all the information in this application form is correct and any grant awarded will be used for the purposes stated.
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-6">
                      <div class="form-group">
                        <label for="date">Date</label>
                        <input type="date" class="form-control" id="date">
                      </div>
                    </div>
                  </div>
                
                  <div class="row">
                    <div class="col-sm-12">
                      <div class="form-group">
                        <label for="signature">Signature of Applicant</label>
                        <textarea id="signature" class="form-control"></textarea>
                      </div>

                      <div class="form-group">
                        <input type="submit" class="btn btn-default" value="Submit My Application"/>
                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>

        </div> 

      </form>    -->    

    </article>

  </section>

<?php endwhile; ?>

<?php get_footer(); ?>