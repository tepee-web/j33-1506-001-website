<?php     
/*
Template Name: Homepage
*/ 
?>

<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); 

  // Banner Content 
  $banner_images = get_field('banner_images', $post_id); 
  
  // Our Mission Section
  $om_content =  get_field('om_content', $post_id);
  $om_images  =  get_field('om_images', $post_id);

  // Our Goals
  $goals  =  get_field('goals', $post_id);

  // We Believe In
  $statements  =  get_field('statements', $post_id);

  // Thirty Three
  $tt_content  =  get_field('tt_content', $post_id);

?>

  <section id="main">

    <!-- Banner -->
    <article id="banner">
      <div class="filter"></div>

      <div class="container">
        <div id="main-carousel" class="carousel slide" data-ride="carousel">

          <!-- Wrapper for slides -->
          <div class="carousel-inner" role="listbox">

            <?php get_banner_images($banner_images); ?>

          </div>

          <!-- Controls -->
          <a class="left carousel-control" href="#main-carousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="right carousel-control" href="#main-carousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>

      </div>
    </article>

    <!--  Introduction -->
    <article id="intro">
    <div class="container center">
      <div class="col-sm-offset-2 col-sm-8">
        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-icon.png" alt="J33"/>
        <?php the_content(); ?>
      </div>
    </div>
    </article>

    <!-- Our Mission -->
    <article id="our-mission">
      <div class="container center">
        <div class="col-sm-offset-2 col-sm-8">
          <h1>Our Mission</h1>
          <p><?php echo $om_content; ?></p>
        </div>
        <div class="col-sm-offset-1 col-sm-10">
          <?php get_two_column_grid($om_images, "Our Mission"); ?>
        </div>
      </div>
    </article>

    <!-- Our Goals -->
    <article id="our-goals">
      <div class="container center">
        <!-- <div class="col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8"> -->
          <h1>Our Goals</h1>
          <ul class="circle-list">
            <?php get_circle_list($goals); ?>
          </ul>
        <!-- </div> -->
      </div>
    </article>

    <!-- Statement of Belief -->
    <article id="statement-of-belief">
      <div class="container center">
          <h1>We Believe In</h1>
          <ul id="tabs" class="nav nav-tabs">
            <?php get_tabbed_circle_list($statements); ?>
          </ul>
          <!-- Tab panes -->
          <div class="tab-content">
            <?php get_tabbed_circle_content($statements); ?>
          </div>
        </div>
      </div>
    </article>

    <!-- Get In Touch< -->
    <article id="thirty-three">
      <div class="container center">
        <div class="circle-container">
          <img src="<?php echo get_template_directory_uri(); ?>/img/thirty-three-logo.png" alt="Thirty Three">
          <?php echo $tt_content; ?>
        </div>
      </div>
    </article>

  </section>

<?php endwhile; ?>

<?php get_footer(); ?>