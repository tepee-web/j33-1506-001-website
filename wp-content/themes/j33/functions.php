<?php

/**
 * @package   WordPress
 * @subpackage  Headspace
 * @version   1.0
 */

//require_once dirname( __FILE__ ) . '/custom/images.php';

function headspace_setup() {

  // disable the admin bar
  show_admin_bar(false);
  
  /* ========================================================================================================================
  Theme specific settings
  Uncomment register_nav_menus to enable a single menu with the title of "Primary Navigation" in your theme
  ======================================================================================================================== */
  
  // activate thumbnails
  add_theme_support('post-thumbnails');


  // Homepage Slider image
  add_image_size( 'homepageBanner', 626, 626);

  // Two column Images
  add_image_size( 'two-col-images', 460, 302, true);


  if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
      'page_title'  => 'Site Details',
      'menu_title'  => 'Edit Site Details',
      'menu_slug'   => 'edit-site-details',
      'capability'  => 'edit_posts',
      'redirect'    => false
    ));
      
  }

  if ( function_exists( 'register_nav_menus' ) ) {
    register_nav_menus(
      array(
        'primary' => 'Primary Header Nav'
      )
    );
  }

}
add_action( 'after_setup_theme', 'headspace_setup' );


// Removes from admin menu
add_action( 'admin_menu', 'my_remove_admin_menus' );
function my_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
}
// Removes from post and pages
add_action('init', 'remove_comment_support', 100);

function remove_comment_support() {
    remove_post_type_support( 'post', 'comments' );
    remove_post_type_support( 'page', 'comments' );
}



  /*
    Display Banners Images
  */
  function get_banner_images($images){

    $counter = 1;
    foreach ($images as $image) { 

      $image_src  = $image['image'];
      $link       = $image['link'];
      $class_name = $counter == 1 ? "item active" : "item";
    ?>
      
      <div class="<?php echo $class_name; ?>">
        <a href="<?php echo $link ?>">
          <img src="<?php echo $image_src['sizes']['homepageBanner']; ?>" alt="Banner Image <?php echo $counter; ?>">
        </a>
      </div>
    
  <?php $counter++; }

  }

  /*
    Display Homepage Two Column Images
  */
  function get_two_column_grid($images, $title){

    foreach ($images as $image) { 

      $image_src  = $image['image'];
    
    ?>
      
      <div class="col-sm-6">
        <img class="img-responsive" src="<?php echo $image_src['sizes']['two-col-images']; ?>" alt="<?php echo $title; ?>">
      </div>
    
  <?php }

  }

  /*
    Display Circle List
  */
  function get_circle_list($lists){
    foreach ($lists as $list_item) { 
      $item_name = "goal";
      $list_text  = $list_item[$item_name]; 
      echo "<li><span>" . $list_text . "</span></li>";
    }
  }

  /*
    Display Circle List
  */
  function get_tabbed_circle_list($lists){
    $counter = 1;
    foreach ($lists as $list_item) { 

      if($counter == 1){
        echo "<li class='active'><a href='#".$counter."' data-toggle='tab'>".$counter."</a></li>"; 
      }
      else{
        echo "<li><a href='#".$counter."' data-toggle='tab'>".$counter."</a></li>"; 
      }
    
      $counter++;   
    }
  }

  function get_tabbed_circle_content($lists){

    $counter = 1;
    foreach ($lists as $list_item) { 
      $item_name = "statement";
      $list_text  = $list_item[$item_name]; 

      if($counter == 1){
        echo "<div role='tabpanel' class='tab-pane active' id='".$counter."'><p>".$list_text."</p></div>"; 
      }
      else{
        echo "<div role='tabpanel' class='tab-pane' id='".$counter."'><p>".$list_text."</p></div>";
      } 
      $counter++;  
    }
  }


  /*
    Display Footer Locations
  */
  function get_addresses(){

    $locations = get_field('addresses', 'option');
    $counter = 0;

    foreach ($locations as $location) {
      $address = $location['address'];

      if($counter == 0){
    ?>
        <div class="col-sm-3 col-md-offset-1 col-md-2">
    <?php }else{ ?>
        <div class="col-sm-3">
    <?php } 
        echo $address;
    ?>
      </div>
      <?php

      $counter++;
    }
    
  }

  /*
    Display Homepage testimonials
  */
  function get_testimonials_count($post_id){

    $testimonials = get_field('testimonials', $post_id);
    $counter      = 0;

    foreach ($testimonials as $testimonial) {

      $is_active = ($counter == 0) ? "active" : ""; ?>
      <li data-target="#testimonials" data-slide-to="<?php echo $counter; ?>" class="<?php echo $is_active; ?>"></li>

      <?php $counter++;
    }
    
  }

  function get_testimonials($post_id){

    $testimonials      = get_field('testimonials', $post_id);
    $counter      = 0;

    foreach ($testimonials as $testimonial) { 
      $is_active  = ($counter == 0) ? "active" : ""; 
      $title      = $testimonial['title']; 
      $content    = $testimonial['content']; 
    ?>

      <div class="item <?php echo $is_active; ?>">
        <h3>"<?php echo $content; ?>"</h3>
        <h4>- <?php echo $title; ?></h4>
      </div>

      <?php $counter++;
    }
    
  }
  


?>