<?php get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

  <section id="main">

    <article id="default-title-area">
      <div class="container">
        <div class="col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8">
          <h1><?php the_title(); ?></h1>
        </div>
      </div>
    </article>

    <!--  Introduction -->
    <article id="default">
    <div class="container">
      <div class="col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8">
        <?php the_content(); ?>
      </div>
    </div>
    </article>

  </section>

<?php endwhile; ?>

<?php get_footer(); ?>