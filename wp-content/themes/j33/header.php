<!DOCTYPE html>
<!--[if lt IE 7]> <html class="lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]> <html class="lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]> <html class="lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html> <!--<![endif]-->
<head>
    <meta charset="utf-8">

    <!-- Title -->
    <title><?php wp_title( 'J33 |' ); ?></title>

    <!-- Pull latest version of IE -->
	  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!-- Set Viewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- IOS Icons -->
    <meta name="apple-mobile-web-app-capable" content="yes">
  	<meta name="format-detection" content="telephone=no">

  	<!-- 16x16 pixels -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
    <!-- 32x32 pixels -->
    <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png">

    <!-- 57x57 (precomposed) for iPhone 3GS, 2011 iPod Touch and older Android devices -->
    <link rel="apple-touch-icon-precomposed" href="<?php echo get_template_directory_uri(); ?>/img/apple-touch-icon-precomposed.png">
    <!-- 72x72 (precomposed) for 1st generation iPad, iPad 2 and iPad mini -->
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_template_directory_uri(); ?>/img/apple-touch-icon-72x72-precomposed.png">
    <!-- 114x114 (precomposed) for iPhone 4, 4S, 5 and 2012 iPod Touch -->
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_template_directory_uri(); ?>/img/apple-touch-icon-114x114-precomposed.png">
    <!-- 144x144 (precomposed) for iPad 3rd and 4th generation -->
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_template_directory_uri(); ?>/img/apple-touch-icon-144x144-precomposed.png">

    <!-- Stylesheets -->
    <link href="<?php echo get_template_directory_uri(); ?>/stylesheets/bootstrap.css" rel="stylesheet" media="screen">
    <link href="<?php echo get_template_directory_uri(); ?>/stylesheets/screen.css" rel="stylesheet" media="screen">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="/wp-content/themes/j33/js/vendor/html5shiv.js"></script>
      <script src="/wp-content/themes/j33/js/vendor/respond.min.js"></script>
    <![endif]-->

	<?php wp_head(); ?>

  </head>
  <body <?php body_class(); ?>>
  <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

    ga('create', 'UA-68014243-1', 'auto');
    ga('send', 'pageview');

  </script>
  <!--
    HEADER
  -->

    <?php
      $email = get_field('email', 'option');
    ?>

    <header id="header">
      <div class="container">
        <div class="row">
          <nav class="navbar navbar-default" role="navigation">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">
                  <img class="icon" src="<?php echo get_template_directory_uri(); ?>/img/header-logo-icon.png" alt="" />
                </a>
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div id="scrollspy" class="collapse navbar-collapse navbar-ex1-collapse">
                <?php
                  $TopMenu = array(
                    'menu'        => 'Main',
                    'container'       => '',
                    'menu_class'      => 'nav navbar-nav navbar-right'
                  );
                  wp_nav_menu( $TopMenu );
                ?>
              </div><!-- /.navbar-collapse -->
          </nav>
        </div>
      </div>
    </header>
