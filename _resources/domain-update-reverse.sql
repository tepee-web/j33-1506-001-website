/* Set up domains */

SET @development = 'j33-1506-001-website.tepee.int';
SET @staging = 'j33-1506-001-website.d.tpsrv.co.uk';
SET @live = 'www.j33.ie';

/* Update staging to live */

UPDATE `wp_postmeta` SET `meta_value` = REPLACE(`meta_value`, @staging, @development);
UPDATE `wp_posts` SET `post_content` = REPLACE(`post_content`, @staging, @development);
UPDATE `wp_options` SET `option_value` = REPLACE(`option_value`, @staging, @development);

/* Update development to staging */

UPDATE `wp_postmeta` SET `meta_value` = REPLACE(`meta_value`, @live, @staging);
UPDATE `wp_posts` SET `post_content` = REPLACE(`post_content`, @live, @staging);
UPDATE `wp_options` SET `option_value` = REPLACE(`option_value`, @live, @staging);